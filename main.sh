#!/bin/bash
set -euo pipefail
# KAIJU Entry Point
# Carlos De Blas <carlos.deblas@databiology.com>
# Databiology @ 2020

SCRATCH=/scratch
SOURCEDIR=/scratch/input
RESULTDIR=/scratch/results
WORKDIR=/scratch/work
DATABASE=/ref/kaiju
PROJECTION=/scratch/projection.json

#Z Functions
falseexit () {
   echo -e "$1"
   exit 1
}

naming () {
    if [ "$OUTPUT" != "null" ]
    then
        OUT="$OUTPUT"
    else
        NAME=$(basename "$R1" | perl -pe 's/_1\.f\w+?$//')
        if [[ $MULTIINPUT != null ]]
        then
            OUT="${NAME}${RANDOM}"
        else
            if [[ -e $RESULTDIR/${NAME}.tsv ]]
            then
                OUT="${NAME}${RANDOM}"
            else
                OUT="${NAME}"
            fi
        fi
    fi
}

# Read parameters from parameters.json
MODE=$(jq -r '.["APPLICATION"] .mode' "$SCRATCH"/parameters.json)
MML="$(jq -r '.["APPLICATION"] .mml' "$SCRATCH"/parameters.json)"
if [ "$MML" != "null" ]; then MINMATCH="-m $MML"; else MINMATCH=''; fi
TAXRANK=$(jq -r '.["APPLICATION"] .tax' "$SCRATCH"/parameters.json)
OUTPUT=$(jq -r '.["APPLICATION"] .out' "$SCRATCH"/parameters.json)

if [[ $MODE == 'greedy' ]]; then
    MSM="$(jq -r '.["APPLICATION"] .msm' "$SCRATCH"/parameters.json)"
    if [ "$MSM" != "null" ]; then MISMATCH="-e $MSM"; else MISMATCH=''; fi
    MTS="$(jq -r '.["APPLICATION"] .mts' "$SCRATCH"/parameters.json)"
    if [ "$MTS" != "null" ]; then MATCHSCORE="-s $MTS"; else MATCHSCORE=''; fi
    EVAL="$(jq -r '.["APPLICATION"] .eval' "$SCRATCH"/parameters.json)"
    if [ "$EVAL" != "null" ]; then EVALUE="-E $EVAL"; else EVALUE=''; fi
else
    MISMATCH=''
    MATCHSCORE=''
    EVALUE=''
fi

MULTIINPUT=$(jq -r '.["WRAPPERCREATOR"] .groupby' $SCRATCH/parameters.json)

# Create workdir
if [ ! -d $WORKDIR ]; then mkdir -p $WORKDIR; fi
cd $WORKDIR || falseexit "Can't move to the work directory"

# Link Fastq and Fasta files to the workdir
while read -r FQ
do
    FQ=$(echo "$FQ" | perl -pe 's/ /\\ /g')
    bash  -c "ln -s $FQ $WORKDIR"
done < <(jq -r '.resources[].relativePath' $PROJECTION)

echo -e "Using: RefSeq Complete Genomes \n \
        \t25M protein sequences from: \n \
        \t\t7065 complete bacterial and archaeal genomes \n \
        \t\t9334 viral genomes \n \
        from NCBI RefSeq. "

# Running Kaiju with PE reads
echo -e "\n\n Running Kaiju with PE reads \n\n"
while read -r R1; do
    naming
    R2=$(echo "$R1" | perl -pe 's/_1\.f/_2.f/')
    if [[ -e $R2 ]]; then
        CMD="kaiju -t $DATABASE/nodes.dmp -f $DATABASE/kaiju_db.fmi -i $R1 -j $R2 -o $RESULTDIR/$OUT.tsv -z $(nproc) -a $MODE \
            $MINMATCH $MISMATCH $MATCHSCORE $EVALUE"
        echo "$CMD"
        $CMD || falseexit "Error while running Kaiju with PE reads"
        rm "$R1" "$R2"
    else
        echo -e "The Read file: <$(basename "$R1")> has no match file\n \
                The file: <$(basename "$R2")> was not found!\n \
                It will run as SE read"
    fi
done < <(jq -r '.resources[].relativePath' $PROJECTION | grep "1_.f")

# Running Kaiju with SE reads
echo -e "\n\n Running Kaiju with SE reads \n\n"
PROC_SE=0
while read -r R1; do
    naming
    CMD="kaiju -t $DATABASE/nodes.dmp -f $DATABASE/kaiju_db.fmi -i $R1 -o $RESULTDIR/$OUT.tsv -z $(nproc) -a $MODE \
        $MINMATCH $MISMATCH $MATCHSCORE $EVALUE"
    echo "$CMD"
    $CMD || falseexit "Error while running Kaiju with SE reads"
    rm "$R1"
    PROC_SE=1
done < <(jq -r '.resources[].relativePath' $PROJECTION | grep ".f")
if [[ $PROC_SE == 0 ]]; then echo -e "\n No SE read files found!\n"; fi

# Creating the classification summary table
echo -e "\n\n Creating classification summary table \n\n"
INPUTS=''
for k in $RESULTDIR/*.tsv; do
    if [[ -e $k ]]; then
        K=$(basename "$k")
        INPUTS="$INPUTS $K"
    fi
done
if [ "$OUTPUT" != "null" ]; then
    OUT="${OUTPUT}_summary_table"
else
    OUT="kaiju_summary_table"
fi
cd $RESULTDIR/ || falseexit "Can't move to the results directory"
CMD2="kaiju2table -t $DATABASE/nodes.dmp -n $DATABASE/names.dmp -r $TAXRANK -o $RESULTDIR/$OUT.tsv $INPUTS"
echo "$CMD2"
$CMD2 || falseexit "Error creating the classification summary table"

