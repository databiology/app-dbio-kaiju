FROM app/dbio/base:4.2.3

# Kaiju Dockerfile
# Carlos de Blas <carlos.deblas@databiology.com>
# (C) 2019 Databiology

# Install dependencies
RUN apt-get update -q=2 && \
    apt-get install -q=2  \
    build-essential git zlib1g-dev && \
    apt-get clean && \
    apt-get purge && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install Kaiju source
RUN mkdir -p /opt/databiology/app && \
    cd /opt/databiology/app && \
    git clone https://github.com/bioinformatics-centre/kaiju.git && \
    cd kaiju/src && \
    make && \
    ln -s /opt/databiology/app/kaiju/bin/* /usr/local/bin/

# Entrypoint definition, /usr/local/bin/main.sh is the default in app/dbio/base
COPY main.sh /usr/local/bin/main.sh
RUN  chmod +x  /usr/local/bin/main.sh

