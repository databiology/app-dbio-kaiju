CIAO: 5


ABOUT:

    NAME:          app/dbio/kaiju
    LABEL:         "Kaiju"
    AUTHOR:        carlos.deblas@databiology.com
    DESCRIPTION:   "Kaiju is a program for the taxonomic classification of high-throughput sequencing reads, e.g., Illumina or Roche/454, from whole-genome sequencing of metagenomic DNA."
    LICENSE:       "GNU General Public License v3.0"

REFERENCES:

    -
        SOURCETREE:  /ref/dbio/kaiju/refseq
        MOUNTPOINT:  /ref/kaiju
        

PARAMETERS:

    -
        TYPE:        CHOICE
        KEY:         db
        LABEL:       "Database"
        VALUE:       "RefSeq Complete Genomes"
        CHOICES:
            - "RefSeq Complete Genomes": "RefSeq (Archaea, Bacteria, and viruses from the NCBI RefSeq database)"
        MODIFIABLE:  False
        DESCRIPTION: "Available Kaiju databases"
        REQUIRED:    False

    -
        TYPE:        CHOICE
        KEY:         mode
        LABEL:       "Run mode"
        VALUE:       "greedy"
        CHOICES:
            - "greedy": "Greedy mode"
            - "mem":       "MEM mode"
        MODIFIABLE:  True
        DESCRIPTION: "Run mode for Kaiju"
        REQUIRED:    False

    -
        TYPE:        CHOICE
        KEY:         tax
        LABEL:       "Taxonomic Rank"
        VALUE:       "species"
        CHOICES:
            - "phylum":   "Phylum"
            - "class":     "Class"
            - "order":     "Order"
            - "family":   "Family"
            - "genus":     "Genus"
            - "species": "Species"
        MODIFIABLE:  True
        DESCRIPTION: "For the classification summary table"
        REQUIRED:    False

    -
        TYPE:        TEXT
        KEY:         mml
        LABEL:       "Min. Match Length"
        VALUE:       "11"
        MODIFIABLE:  True
        DESCRIPTION: "Minimum match length, default: 11."
        REQUIRED:    False

    -
        TYPE:        TEXT
        KEY:         out
        LABEL:       "Output Name"
        VALUE:       ""
        MODIFIABLE:  True
        DESCRIPTION: "Name for the output, note that if there are multiple files it should be left blank."
        REQUIRED:    False

    -
        TYPE:        TEXT
        KEY:         msm
        LABEL:       "Mismatches Allowed"
        VALUE:       "3"
        MODIFIABLE:  True
        DESCRIPTION: "Number of mismatches allowed in Greedy mode, default: 3."
        REQUIRED:    False

    -
        TYPE:        TEXT
        KEY:         mts
        LABEL:       "Min. Match Score"
        VALUE:       "65"
        MODIFIABLE:  True
        DESCRIPTION: "Minimum match score in Greedy mode, default: 65."
        REQUIRED:    False

    -
        TYPE:        TEXT
        KEY:         eval
        LABEL:       "E-value"
        VALUE:       ""
        MODIFIABLE:  True
        DESCRIPTION: "Minimum E-value in Greedy mode, e.g. 0.05"
        REQUIRED:    False

